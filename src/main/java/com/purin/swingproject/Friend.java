/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.purin.swingproject;

/**
 *
 * @author tin
 */
public class Friend {
    private String name;
    private int age;
    private String gender;
    private String descripption;

    public Friend(String name, int age, String gender, String descripption) {
        this.name = name;
        this.age = age;
        this.gender = gender;
        this.descripption = descripption;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDescripption() {
        return descripption;
    }

    public void setDescripption(String descripption) {
        this.descripption = descripption;
    }

    @Override
    public String toString() {
        return "Friend{" + "name=" + name + ", age=" + age + ", gender=" + gender + ", descripption=" + descripption + '}';
    }
    
}
